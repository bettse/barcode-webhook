require('dotenv').config()
const SerialPort = require("serialport");
const Readline = require("@serialport/parser-readline");
const fetch = require('node-fetch');

const { DEBUG, SERIAL_PORT, WEBHOOK } = process.env;

if (!SERIAL_PORT) {
  console.log("SERIAL_PORT not defined");
  process.exit(1);
}

const port = new SerialPort(SERIAL_PORT, { baudRate: 9600 });
const parser = port.pipe(new Readline({ delimiter: '\r\n' }));

port.on("error", err => {
  console.error(err);
});

port.on("open", () => {
  console.log("ready");
});

parser.on("data", async data => {
  console.log({ data });

  const response = await fetch(WEBHOOK, {
    method: "POST",
    body: JSON.stringify({id: data}),
  });
  const body = await response.text();
  if (!response.ok) {
    console.log('webhook error', body);
  }
});
