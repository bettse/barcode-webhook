# barcode-webhook

Open a serial port connected to a barcode scanner and post the data to a webhook.  In my case I'm using a Intermec SR61t in Virtual COM mode and pushing the data to [Sockethook](https://sockethook.ericbetts.dev/).

## Setup

 * Define SERIAL_PORT env (or in .env)
 * Define WEBHOOK env (or in .env)

## Run

 * `node index`

## Auto start

```sh
sudo cp barcode.service /lib/systemd/system/
sudo chmod 644 /lib/systemd/system/barcode.service
sudo systemctl daemon-reload
sudo systemctl enable barcode.service
sudo systemctl start barcode.service
```

### Monitor

`journalctl --since today -f -u barcode.service`


